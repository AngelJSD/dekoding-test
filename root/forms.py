from django import forms

from django.forms import ModelForm, TextInput, NumberInput, CharField, IntegerField, ModelChoiceField
from .models import DogModel, OwnerModel

class OwnerModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        #Show the name and the last name of each owner
        return obj.owner_name + " " + obj.last_name

class DogForm(ModelForm):

    owner = OwnerModelChoiceField(queryset=OwnerModel.objects.all(), empty_label='Owner', widget=forms.Select(attrs={'class': 'form-control'}))
    
    class Meta:
        model = DogModel
        fields = ('name', 'age', 'breed', 'owner')
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Name', 'class': 'form-control'}),
            'age': NumberInput(attrs={'placeholder': 'Age', 'class': 'form-control', 'min': '0'}),
            'breed': TextInput(attrs={'placeholder': 'Breed', 'class': 'form-control'}),
        }
        
class OwnerForm(ModelForm):
    class Meta:
        model = OwnerModel
        fields = ('owner_name', 'last_name', 'owner_age', 'country')
        widgets = {
            'owner_name': TextInput(attrs={'placeholder': 'Name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Last Name', 'class': 'form-control'}),
            'country': TextInput(attrs={'placeholder': 'Country', 'class': 'form-control'}),
            'owner_age': NumberInput(attrs={'placeholder': 'Age', 'class': 'form-control', 'min': '0'})
        }