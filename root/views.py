from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import View

from .models import DogModel, OwnerModel
from .forms import DogForm, OwnerForm

class IndexView(View):
    template_name = "index.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class FrontendView(View):
    template_name = "frontend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class BackendView(View):
    template_name = "backend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)

class DogList(View):
    template_name = "dog/list.html"

    def get(self, request):
        #Obtain all the dogs
        dogs = DogModel.objects.all()
        #Save them as a context to send to the frontend
        context = {'dogs': dogs}

        return render(request, self.template_name, context)

class DogAdd(View):
    template_name = "dog/add.html"

    def get(self, request):
        
        #Generate the form
        form = DogForm()
        #Send the form as context to the frontend
        return render(request, self.template_name, {'form': form})
    
    def post(self, request, **kwargs):
        
        #Obtain the data from the request
        form = DogForm(request.POST)
        
        #Simple validation of the data
        if form.is_valid():
            #Save the data in the database
            dog = form.save(commit=False)
            dog.save()

            return HttpResponseRedirect('/dog/list/')

class OwnerList(View):
    template_name = "owner/list.html"

    def get(self, request):
        #Obtain all the owners
        owners = OwnerModel.objects.all()
        context = {'owners': owners}
        #Send them as context to the frontend
        return render(request, self.template_name, context)

class OwnerAdd(View):
    template_name = "owner/add.html"

    def get(self, request):
        
        #Generate the form
        form = OwnerForm()
        #Send the form as context to the frontend
        return render(request, self.template_name, {'form': form})
    
    def post(self, request, **kwargs):
        
        #Obtain the data from the request
        form = OwnerForm(request.POST)

        #Simple validation of the data
        if form.is_valid():
            #Save the data in the database
            owner = form.save(commit=False)
            owner.save()
            
            return HttpResponseRedirect('/owner/list/')

