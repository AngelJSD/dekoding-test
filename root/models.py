from django.db import models

class OwnerModel(models.Model):
    owner_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    country = models.CharField(max_length=30)
    owner_age = models.IntegerField()

class DogModel(models.Model):
    name = models.CharField(max_length=30)
    age = models.IntegerField()
    breed = models.CharField(max_length=30)
    owner = models.ForeignKey(OwnerModel, on_delete=models.CASCADE)
    
