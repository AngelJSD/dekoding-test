from django.urls import path

from .views import (
    IndexView,
    FrontendView,
    BackendView,
    DogAdd,
    DogList,
    OwnerAdd,
    OwnerList
)


urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('frontend/', FrontendView.as_view(), name="frontend"),
    path('backend/', BackendView.as_view(), name="backend"),
    path('dog/add/', DogAdd.as_view(), name="add_dog"),
    path('dog/list/', DogList.as_view(), name="list_dog"),
    path('owner/add/', OwnerAdd.as_view(), name="add_owner"),
    path('owner/list/', OwnerList.as_view(), name="list_owner")
]
